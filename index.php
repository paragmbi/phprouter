<?php
define('DS', DIRECTORY_SEPARATOR, true);
define('BASE_PATH', __DIR__ . DS, TRUE);

require BASE_PATH.'vendor/autoload.php';

function function1() {
    echo 'Page Content';
}

class home
{
    function pages() {
        echo 'Home page Content';
    }
}

$app            = System\App::instance();
$app->request   = System\Request::instance();
$app->route     = System\Route::instance($app->request);

$route          = $app->route;

$route->any('/', function() {
    //echo 'Hello World';
    $loader = new Twig_Loader_Filesystem(BASE_PATH.'templates/');

    $twig = new Twig_Environment($loader, array(
        'cache' => false,
    ));
    $template = $twig->load('sample/index.html');
    echo $template->render();

});

$route->any('/about', function() {
    echo 'About';
});

$route->any('/function1','function1');

//$route->get('/test', ['home', 'pages']);

// OR

//$home = new home;
//$route->get('/test', [$home, 'pages']);

// OR

$route->get('/test', 'home@pages');

$route->get('/homec', 'App\homeController@index');

$route->group('/admin', function()
{
    // /admin/
    $this->get('/', function() {
        echo 'welcome to admin panel';
    });

    // /admin/settings
    $this->get('/settings', function() {
        echo 'list of settings';
    });

    // nested group
    $this->group('/users', function()
    {
        // /admin/users
        $this->get('/', function() {
            echo 'list of users';
        });

        // /admin/users/add
        $this->get('/add', function() {
            echo 'add new user';
        });
    });

    // Anything else
    $this->any('/*', function() {
        pre("Page ( {$this->app->request->path} ) Not Found", 6);
    });
});

$route->end();